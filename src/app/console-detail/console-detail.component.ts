import { Component, Input, OnInit } from '@angular/core'
import { Console } from '../domain/Console'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { ConsoleService } from '../console.service'

@Component({
  selector: 'app-console-detail',
  templateUrl: './console-detail.component.html',
  styleUrls: ['./console-detail.component.scss']
})
export class ConsoleDetailComponent implements OnInit {
  @Input() console: Console

  constructor(
    private route: ActivatedRoute,
    private consoleService: ConsoleService,
    private location: Location
  ) {}

  goBack(): void {
    this.location.back()
  }

  getConsoleId(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.consoleService.getConsoleId(id).subscribe(console => (this.console = console))
  }

  deleteConsole(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.consoleService.deleteConsole(id).subscribe()
    this.goBack()
  }

  ngOnInit() {
    this.getConsoleId()
  }
}
