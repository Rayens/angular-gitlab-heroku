import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ConsoleDetailComponent } from './console-detail.component'
import { ControlContainer } from '@angular/forms'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('ConsoleDetailComponent', () => {
  let component: ConsoleDetailComponent
  let fixture: ComponentFixture<ConsoleDetailComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsoleDetailComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: ControlContainer },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsoleDetailComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
