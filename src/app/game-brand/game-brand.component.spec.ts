import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { GameBrandComponent } from './game-brand.component'
import { ControlContainer } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { RouterTestingModule } from '@angular/router/testing'

describe('GameBrandComponent', () => {
  let component: GameBrandComponent
  let fixture: ComponentFixture<GameBrandComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameBrandComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        { provide: ControlContainer },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBrandComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
