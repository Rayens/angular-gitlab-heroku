import { Component, OnInit } from '@angular/core'
import { GameBrand } from '../domain/GameBrand'
import { GameBrandService } from '../game-brand.service'

@Component({
  selector: 'app-game-brand',
  templateUrl: './game-brand.component.html',
  styleUrls: ['./game-brand.component.scss']
})
export class GameBrandComponent implements OnInit {
  gamebrands: GameBrand[]

  constructor(private gs: GameBrandService) {}

  getGamebrands(): void {
    this.gs.getGamebrand().subscribe(gamebrands => (this.gamebrands = gamebrands))
  }

  ngOnInit() {
    this.getGamebrands()
  }
}
