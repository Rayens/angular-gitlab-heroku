export class Game {
  name: string
  // tslint:disable-next-line:variable-name
  _id: number
  ean: number
  console: string
  pegi: number
  genre: string
  gamebrand: string
  released: Date
  description: string
  isSelected: boolean
}
