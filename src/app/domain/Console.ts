export class Console {
  name: string
  // tslint:disable-next-line:variable-name
  _id: string
  brand: string
  model: string
  hdd: string
  processor: string
  ram: string
  released: Date
  description: string
  isSelected: boolean
}
