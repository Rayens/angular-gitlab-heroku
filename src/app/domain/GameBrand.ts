export class GameBrand {
  // tslint:disable-next-line:variable-name
  _id: string
  name: string
  owner: string
  introductionYear: Date
  description: string
  history: string
  isSelected: boolean
}
