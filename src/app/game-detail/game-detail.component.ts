import { Component, Input, OnInit } from '@angular/core'
import { Game } from '../domain/Game'
import { ActivatedRoute } from '@angular/router'
import { GameService } from '../game.service'
import { Location } from '@angular/common'
import { GameBrandService } from '../game-brand.service'
import { GameBrand } from '../domain/GameBrand'

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {
  @Input() game: Game
  @Input() gamebrand: GameBrand

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private gs: GameService,
    private gbs: GameBrandService
  ) {}

  goBack(): void {
    this.location.back()
  }

  getGameId(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.gs.getGameId(id).subscribe(game => (this.game = game))
  }

  deleteGame(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.gs.deleteGame(id).subscribe()
    this.goBack()
  }

  ngOnInit() {
    this.getGameId()
  }
}
