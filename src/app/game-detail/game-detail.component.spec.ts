import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { GameDetailComponent } from './game-detail.component'
import { ActivatedRoute, RouterModule } from '@angular/router'
import { Location } from '@angular/common'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { AppComponent } from '../core/app/app.component'

describe('GameDetailComponent', () => {
  let component: GameDetailComponent
  let fixture: ComponentFixture<GameDetailComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameDetailComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GameDetailComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
