import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { GameBrandUpdateComponent } from './game-brand-update.component'
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars'
import { ControlContainer, FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'

describe('GameBrandUpdateComponent', () => {
  let component: GameBrandUpdateComponent
  let fixture: ComponentFixture<GameBrandUpdateComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameBrandUpdateComponent],
      imports: [DatePickerModule, FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      providers: [
        { provide: ControlContainer },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBrandUpdateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
