import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { DatePickerComponent } from '@syncfusion/ej2-angular-calendars'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { GameBrand } from '../domain/GameBrand'
import { GameBrandService } from '../game-brand.service'

@Component({
  selector: 'app-game-brand-update',
  templateUrl: './game-brand-update.component.html',
  styleUrls: ['./game-brand-update.component.scss']
})
export class GameBrandUpdateComponent implements OnInit {
  gamebrand: GameBrand
  angForm: FormGroup

  constructor(
    private fb: FormBuilder,
    private gs: GameBrandService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.createForm()
  }

  createForm() {
    this.angForm = this.fb.group({
      gamebrandName: [''],
      gamebrandOwner: [''],
      gamebrandIntroductionyear: ['', Validators.required],
      gamebrandDescription: [''],
      gamebrandHistory: ['']
    })
  }

  goBack(): void {
    this.location.back()
  }

  get datepicker() {
    return this.angForm.get('gamebrandIntroductionyear')
  }

  getGamebrandId(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.gs.getGamebrandId(id).subscribe(gamebrands => (this.gamebrand = gamebrands))
  }

  updateGamebrand(
    gamebrandName,
    gamebrandOwner,
    gamebrandIntroductionyear,
    gamebrandDescription,
    gamebrandHistory
  ) {
    const id = this.route.snapshot.paramMap.get('id')
    this.gs.updateGamebrand(
      id,
      gamebrandName,
      gamebrandOwner,
      gamebrandIntroductionyear,
      gamebrandDescription,
      gamebrandHistory
    )
    this.goBack()
  }

  ngOnInit() {
    this.getGamebrandId()
  }
}
