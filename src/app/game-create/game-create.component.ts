import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { DatePickerComponent } from '@syncfusion/ej2-angular-calendars'
import { Console } from '../domain/Console'
import { GameBrand } from '../domain/GameBrand'
import { GameService } from '../game.service'
import { ConsoleService } from '../console.service'
import { GameBrandService } from '../game-brand.service'
import { CheckBoxSelectionService } from '@syncfusion/ej2-angular-dropdowns'
import { Location } from '@angular/common'

@Component({
  selector: 'app-game-create',
  templateUrl: './game-create.component.html',
  styleUrls: ['./game-create.component.scss'],
  providers: [CheckBoxSelectionService]
})
export class GameCreateComponent implements OnInit {
  angForm: FormGroup
  ajDatePicker: DatePickerComponent
  consoles: Console[]
  gamebrand: GameBrand[]
  public mode: string

  constructor(
    private fb: FormBuilder,
    private gs: GameService,
    private cs: ConsoleService,
    private gbs: GameBrandService,
    private location: Location
  ) {
    this.createForm()
  }

  goBack(): void {
    this.location.back()
  }

  getConsoles(): void {
    this.cs.getConsole().subscribe(consoles => (this.consoles = consoles))
  }

  getGamebrands(): void {
    this.gbs.getGamebrand().subscribe(gamebrands => (this.gamebrand = gamebrands))
  }

  createForm() {
    this.angForm = this.fb.group({
      gameName: ['', Validators.required],
      gameEan: ['', Validators.required],
      gameConsole: ['', Validators.required],
      gamePegi: ['', Validators.required],
      gameGamebrand: ['', Validators.required],
      gameGenre: ['', Validators.required],
      gameReleased: ['', Validators.required],
      gameDescription: ['', Validators.required]
    })
  }

  get datepicker() {
    return this.angForm.get('gameReleased')
  }

  addGame(gameName, gameEan, gameConsole, gamePegi, gameGenre, gameGamebrand, gameReleased, gameDescription) {
    this.gs.addGame(
      gameName,
      gameEan,
      gameConsole,
      gamePegi,
      gameGenre,
      gameGamebrand,
      gameReleased,
      gameDescription
    )
    this.goBack()
  }

  ngOnInit() {
    this.getConsoles()
    this.getGamebrands()
    this.mode = 'CheckBox'
  }
}
