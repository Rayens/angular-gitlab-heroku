import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Registreren',
      description: 'Hiermee registreert een gebruiker zich.',
      scenario: [
        'Gebruiker vult persoonlijke informatie in en klikt op de registreer knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien alle velden kloppen wordt er een nieuwe gebruiker in de database gezet.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is geregistreert'
    },
    {
      id: 'UC-02',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-03',
      name: 'Uitloggen',
      description: 'Hiermee logt een bestaande gebruiker uit.',
      scenario: [
        'Gebruiker klikt op de button uitloggen.',
        'de applicatie redirect uitgelogt naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De actor is uitgelogd'
    },
    {
      id: 'UC-04',
      name: 'Game toevoegen',
      description: 'Hiermee kan een ingelogde gebruiker een Game toevoegen',
      scenario: [
        'Gebruiker vult de specificaties in van een game',
        'De applicatie verifieert of de verplichte velden zijn ingevuld',
        'Als alles klopt wordt er een game toegevoegd'
      ],
      actor: 'ingelogde gebruiker',
      precondition: 'De ingelogde gebruiker is ingelogd',
      postcondition: 'De ingelogde gebruiker heeft een game toegevoegd.'
    },
    {
      id: 'UC-05',
      name: 'Game bijwerken',
      description: 'Hiermee kan een ingelogde gebruiker een Game bijwerken',
      scenario: [
        'Gebruiker vult de velden in in van een game die hij wilt aanpassen',
        'De applicatie verifieert of de verplichte velden zijn ingevuld',
        'Als alles klopt wordt er een game bijgewerkt'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'De ingelogde gebruiker heeft een game bijgewerkt.'
    },
    {
      id: 'UC-06',
      name: 'Game verwijderen',
      description: 'Hiermee kan een ingelogde gebruiker een Game verwijderen',
      scenario: [
        'Gebruiker klikt op de knop delete',
        'De applicatie verifieert of de game in de database staat',
        'Als alles klopt wordt er een game verwijdert'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'De ingelogde gebruiker heeft een game verwijderd.'
    },
    {
      id: 'UC-07',
      name: 'Console toevoegen',
      description: 'Hiermee kan een ingelogde gebruiker een Console toevoegen',
      scenario: [
        'Gebruiker vult de specificaties in van een Console',
        'De applicatie verifieert of de verplichte velden zijn ingevuld',
        'Als alles klopt wordt er een Console toegevoegd'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'De ingelogde gebruiker heeft een Console toegevoegd.'
    },
    {
      id: 'UC-08',
      name: 'Console bijwerken',
      description: 'Hiermee kan een ingelogde gebruiker een Console bijwerken',
      scenario: [
        'Gebruiker vult de velden in in van een Console die hij wilt aanpassen',
        'De applicatie verifieert of de verplichte velden zijn ingevuld',
        'Als alles klopt wordt er een Console bijgewerkt'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'De ingelogde gebruiker heeft een Console bijgewerkt.'
    },
    {
      id: 'UC-09',
      name: 'Console verwijderen',
      description: 'Hiermee kan een ingelogde gebruiker een Console verwijderen',
      scenario: [
        'Gebruiker klikt op de knop delete in de detail pagina van een console',
        'De applicatie verifieert of de console in de database staat',
        'Als alles klopt wordt er een console verwijdert'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'De ingelogde gebruiker heeft een console verwijderd.'
    },
    {
      id: 'UC-10',
      name: 'Game brand toevoegen',
      description: 'Hiermee kan een ingelogde gebruiker een Game brand toevoegen',
      scenario: [
        'Gebruiker vult de velden in van een Game brand',
        'De applicatie verifieert of de verplichte velden zijn ingevuld',
        'Als alles klopt wordt er een Game brand toegevoegd'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'De ingelogde gebruiker heeft een Game brand toegevoegd.'
    },
    {
      id: 'UC-11',
      name: 'Game brand bijwerken',
      description: 'Hiermee kan een ingelogde gebruiker een Game brand bijwerken',
      scenario: [
        'Gebruiker vult de velden in in van een Game brand die hij wilt aanpassen',
        'De applicatie verifieert of de verplichte velden zijn ingevuld',
        'Als alles klopt wordt er een Game brand bijgewerkt'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'De ingelogde gebruiker heeft een Game brand bijgewerkt.'
    },
    {
      id: 'UC-12',
      name: 'Game brand verwijderen',
      description: 'Hiermee kan een ingelogde gebruiker een Game brand verwijderen',
      scenario: [
        'Gebruiker klikt op de knop delete',
        'De applicatie verifieert of de game in de database staat',
        'Als alles klopt wordt er een Game brand verwijdert'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'De ingelogde gebruiker heeft een Game brand verwijderd.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
