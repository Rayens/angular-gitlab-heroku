import { TestBed } from '@angular/core/testing'

import { GameBrandService } from './game-brand.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('GameBrandService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    })
  )

  it('should be created', () => {
    const service: GameBrandService = TestBed.get(GameBrandService)
    expect(service).toBeTruthy()
  })
})
