import { TestBed } from '@angular/core/testing'

import { ConsoleService } from './console.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('ConsoleService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    })
  )

  it('should be created', () => {
    const service: ConsoleService = TestBed.get(ConsoleService)
    expect(service).toBeTruthy()
  })
})
