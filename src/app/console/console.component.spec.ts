import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ConsoleComponent } from './console.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'

describe('ConsoleComponent', () => {
  let component: ConsoleComponent
  let fixture: ComponentFixture<ConsoleComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsoleComponent],
      imports: [HttpClientTestingModule, RouterTestingModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsoleComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
