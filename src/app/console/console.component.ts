import { Component, OnInit } from '@angular/core'
import { Console } from '../domain/Console'
import { ConsoleService } from '../console.service'

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.scss']
})
export class ConsoleComponent implements OnInit {
  Consoles: Console[]
  selectedConsole: Console

  getConsole(): void {
    this.consoleService.getConsole().subscribe(consoles => (this.Consoles = consoles))
  }

  constructor(private consoleService: ConsoleService) {}

  ngOnInit() {
    this.getConsole()
  }
}
