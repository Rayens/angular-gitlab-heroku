import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { DatePickerComponent } from '@syncfusion/ej2-angular-calendars'
import { ConsoleService } from '../console.service'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { Console } from '../domain/Console'

@Component({
  selector: 'app-console-update',
  templateUrl: './console-update.component.html',
  styleUrls: ['./console-update.component.scss']
})
export class ConsoleUpdateComponent implements OnInit {
  console: Console
  angForm: FormGroup
  ejDatePicker: DatePickerComponent

  constructor(
    private fb: FormBuilder,
    private cs: ConsoleService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.createForm()
  }

  createForm() {
    this.angForm = this.fb.group({
      console_name: [''],
      console_brand: [''],
      console_model: [''],
      console_hdd: [''],
      console_processor: [''],
      console_ram: [''],
      console_date: ['', Validators.required],
      console_description: ['']
    })
  }

  goBack(): void {
    this.location.back()
  }

  get datepicker() {
    return this.angForm.get('console_date')
  }

  getConsoleId(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.cs.getConsoleId(id).subscribe(console => (this.console = console))
  }

  // tslint:disable-next-line:variable-name max-line-length
  updateConsole(
    // tslint:disable-next-line:variable-name
    console_name,
    // tslint:disable-next-line:variable-name
    console_brand,
    // tslint:disable-next-line:variable-name
    console_model,
    // tslint:disable-next-line:variable-name
    console_hdd,
    // tslint:disable-next-line:variable-name
    console_processor,
    // tslint:disable-next-line:variable-name
    console_ram,
    // tslint:disable-next-line:variable-name
    console_date,
    // tslint:disable-next-line:variable-name
    console_description
  ) {
    const id = this.route.snapshot.paramMap.get('id')
    this.cs.updateConsole(
      id,
      console_name,
      console_brand,
      console_model,
      console_hdd,
      console_processor,
      console_ram,
      console_date,
      console_description
    )
    this.goBack()
  }

  ngOnInit() {
    this.getConsoleId()
  }
}
