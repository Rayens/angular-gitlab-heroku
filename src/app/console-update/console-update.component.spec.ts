import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ConsoleUpdateComponent } from './console-update.component'
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars'
import { ControlContainer, FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'

describe('ConsoleUpdateComponent', () => {
  let component: ConsoleUpdateComponent
  let fixture: ComponentFixture<ConsoleUpdateComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsoleUpdateComponent],
      imports: [DatePickerModule, FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      providers: [
        { provide: ControlContainer },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsoleUpdateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
