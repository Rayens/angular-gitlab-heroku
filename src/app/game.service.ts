import { Injectable } from '@angular/core'
import { Game } from './domain/Game'
import { Observable, observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private gameUrl = 'api/game'

  constructor(private http: HttpClient) {}

  addGame(gameName, gameEan, gameConsole, gamePegi, gameGenre, gameGamebrand, gameRelease, gameDescription) {
    const game = {
      name: gameName,
      ean: gameEan,
      console: gameConsole,
      pegi: gamePegi,
      genre: gameGenre,
      gamebrand: gameGamebrand,
      released: gameRelease,
      description: gameDescription
    }
    this.http.post(this.gameUrl, game).subscribe()
  }

  getGame(): Observable<Game[]> {
    return this.http.get<Game[]>(this.gameUrl)
  }

  getGameId(id): Observable<Game> {
    const url = `${this.gameUrl}/${id}`
    return this.http.get<Game>(url)
  }

  deleteGame(id): Observable<Game> {
    const url = `${this.gameUrl}/${id}`
    return this.http.delete<Game>(url)
  }

  updateGame(
    id,
    gameName,
    gameEan,
    gameConsole,
    gamePegi,
    gameGenre,
    gameGamebrand,
    gameRelease,
    gameDescription
  ) {
    const url = `${this.gameUrl}/${id}`
    const updatedGame = {
      name: gameName,
      ean: gameEan,
      console: gameConsole,
      pegi: gamePegi,
      genre: gameGenre,
      gamebrand: gameGamebrand,
      released: gameRelease,
      description: gameDescription
    }
    this.http.put(url, updatedGame).subscribe()
  }
}
