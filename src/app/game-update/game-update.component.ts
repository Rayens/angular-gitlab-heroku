import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { GameService } from '../game.service'
import { Game } from '../domain/Game'
import { Console } from '../domain/Console'
import { GameBrand } from '../domain/GameBrand'
import { ConsoleService } from '../console.service'
import { GameBrandService } from '../game-brand.service'

@Component({
  selector: 'app-game-update',
  templateUrl: './game-update.component.html',
  styleUrls: ['./game-update.component.scss']
})
export class GameUpdateComponent implements OnInit {
  angForm: FormGroup
  game: Game
  consoles: Console[]
  gamebrand: GameBrand[]

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private location: Location,
    private gs: GameService,
    private cs: ConsoleService,
    private gbs: GameBrandService
  ) {
    this.createForm()
  }

  goBack(): void {
    this.location.back()
  }

  getGameId(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.gs.getGameId(id).subscribe(games => (this.game = games))
  }

  getConsoles(): void {
    this.cs.getConsole().subscribe(consoles => (this.consoles = consoles))
  }

  getGamebrands(): void {
    this.gbs.getGamebrand().subscribe(gamebrands => (this.gamebrand = gamebrands))
  }

  createForm() {
    this.angForm = this.fb.group({
      gameName: [''],
      gameEan: [''],
      gameConsole: ['', Validators.required],
      gamePegi: [''],
      gameGamebrand: ['', Validators.required],
      gameGenre: ['', Validators.required],
      gameReleased: ['', Validators.required],
      gameDescription: ['']
    })
  }

  get datepicker() {
    return this.angForm.get('gameReleased')
  }

  UpdateGame(
    gameName,
    gameEan,
    gameConsole,
    gamePegi,
    gameGenre,
    gameGamebrand,
    gameReleased,
    gameDescription
  ) {
    const id = this.route.snapshot.paramMap.get('id')
    this.gs.updateGame(
      id,
      gameName,
      gameEan,
      gameConsole,
      gamePegi,
      gameGenre,
      gameGamebrand,
      gameReleased,
      gameDescription
    )
    this.goBack()
  }

  ngOnInit() {
    this.getGameId()
    this.getConsoles()
    this.getGamebrands()
  }
}
