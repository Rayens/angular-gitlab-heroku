import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { GameUpdateComponent } from './game-update.component'
import { ControlContainer, FormsModule, ReactiveFormsModule } from '@angular/forms'
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('GameUpdateComponent', () => {
  let component: GameUpdateComponent
  let fixture: ComponentFixture<GameUpdateComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameUpdateComponent],
      imports: [DatePickerModule, FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      providers: [
        { provide: ControlContainer },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GameUpdateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
