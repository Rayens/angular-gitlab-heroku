import { Injectable } from '@angular/core'
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'

import { AuthenticationService } from '../authentication/_services/authentication.service'
import { Router } from '@angular/router'

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(err => {
        if (err.status === 401) {
          // auto logout if 401 response returned from api
          this.authenticationService.logout()
          location.reload(true)
        } else if (err.status === 404) {
          this.router.navigate(['error/404'], err.error)
        } else if (err.status === 500) {
          this.router.navigate(['error/500'], err.error)
        } else if (err.status === 403) {
          this.router.navigate(['error/403'], err.error)
        } else {
          this.router.navigate(['error/500'], err.error)
        }

        const error = err.error.message || err.statusText
        return throwError(error)
      })
    )
  }
}
