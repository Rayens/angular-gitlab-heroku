/* tslint:disable:no-string-literal */
import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { GameBrandCreateComponent } from './game-brand-create.component'
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars'
import { ControlContainer, FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'

describe('GameBrandCreateComponent', () => {
  let component: GameBrandCreateComponent
  let fixture: ComponentFixture<GameBrandCreateComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameBrandCreateComponent],
      imports: [DatePickerModule, FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      providers: [
        { provide: ControlContainer },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBrandCreateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  function createForm(name, owner, year, description, history) {
    component.angForm.controls['gamebrandName'].setValue(name)
    component.angForm.controls['gamebrandOwner'].setValue(owner)
    component.angForm.controls['gamebrandIntroductionyear'].setValue(year)
    component.angForm.controls['gamebrandDescription'].setValue(description)
    component.angForm.controls['gamebrandHistory'].setValue(history)
  }

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })

  it('should render title in a h2 tag', async(() => {
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelector('h2').textContent).toContain('Create Game Brand')
  }))

  it('component initial state', () => {
    expect(component.angForm).toBeDefined()
    expect(component.angForm.invalid).toBeTruthy()
  })

  it('form value should update from when u change the input', () => {
    const validBrand = {
      gamebrandName: 'gamebrand',
      gamebrandOwner: 'owner',
      gamebrandIntroductionyear: '10/10/2010',
      gamebrandDescription: 'mooie gamebrand',
      gamebrandHistory: 'gamebrand history'
    }
    createForm(
      validBrand.gamebrandName,
      validBrand.gamebrandOwner,
      validBrand.gamebrandIntroductionyear,
      validBrand.gamebrandDescription,
      validBrand.gamebrandHistory
    )
    expect(component.angForm.value).toEqual(validBrand)
  })

  it('Form invalid should be true when form is invalid', () => {
    const blankBrand = {
      gamebrandName: '',
      gamebrandOwner: '',
      gamebrandIntroductionyear: '',
      gamebrandDescription: '',
      gamebrandHistory: ''
    }

    createForm(
      blankBrand.gamebrandName,
      blankBrand.gamebrandOwner,
      blankBrand.gamebrandIntroductionyear,
      blankBrand.gamebrandDescription,
      blankBrand.gamebrandHistory
    )
    expect(component.angForm.invalid).toBeTruthy()
  })
})
