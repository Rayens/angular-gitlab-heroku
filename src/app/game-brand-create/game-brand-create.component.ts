import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { DatePickerComponent } from '@syncfusion/ej2-angular-calendars'
import { GameBrandService } from '../game-brand.service'
import { Location } from '@angular/common'

@Component({
  selector: 'app-game-brand-create',
  templateUrl: './game-brand-create.component.html',
  styleUrls: ['./game-brand-create.component.scss']
})
export class GameBrandCreateComponent implements OnInit {
  angForm: FormGroup
  ajDatePicker: DatePickerComponent

  constructor(private fb: FormBuilder, private gs: GameBrandService, private location: Location) {
    this.createForm()
  }

  createForm() {
    this.angForm = this.fb.group({
      gamebrandName: ['', Validators.required],
      gamebrandOwner: ['', Validators.required],
      gamebrandIntroductionyear: ['', Validators.required],
      gamebrandDescription: ['', Validators.required],
      gamebrandHistory: ['', Validators.required]
    })
  }

  goBack(): void {
    this.location.back()
  }

  get datepicker() {
    return this.angForm.get('gamebrandIntroductionyear')
  }

  addGamebrand(
    gamebrandName,
    gamebrandOwner,
    gamebrandIntroductionyear,
    gamebrandDescription,
    gamebrandHistory
  ) {
    this.gs.addGamebrand(
      gamebrandName,
      gamebrandOwner,
      gamebrandIntroductionyear,
      gamebrandDescription,
      gamebrandHistory
    )
    this.goBack()
  }
  ngOnInit() {}
}
