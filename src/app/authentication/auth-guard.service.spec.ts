import { TestBed } from '@angular/core/testing'

import { AuthGuardService } from './auth-guard.service'
import { AuthService } from './auth.service'
import { JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { RouterTestingModule } from '@angular/router/testing'

describe('AuthGuardService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        AuthService,
        AuthGuardService,
        JwtHelperService,
        { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    })
  )

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService)
    expect(service).toBeTruthy()
  })
})
