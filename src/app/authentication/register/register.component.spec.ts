/* tslint:disable:no-string-literal */
import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { RegisterComponent } from './register.component'
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars'
import { ControlContainer, FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'

describe('RegisterComponent', () => {
  let component: RegisterComponent
  let fixture: ComponentFixture<RegisterComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [FormsModule, ReactiveFormsModule, HttpClientTestingModule, RouterTestingModule],
      providers: [
        { provide: ControlContainer },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  function registerForm(userName, userEmail, userPassword) {
    component.registerForm.controls['name'].setValue(userName)
    component.registerForm.controls['email'].setValue(userEmail)
    component.registerForm.controls['password'].setValue(userPassword)
  }

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })

  it('should render title in a h2 tag', async(() => {
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelector('h2').textContent).toContain('Register')
  }))

  it('component initial state', () => {
    expect(component.submitted).toBeFalsy()
    expect(component.registerForm).toBeDefined()
    expect(component.registerForm.invalid).toBeTruthy()
  })

  it('form value should update from when u change the input', () => {
    const validUser = {
      name: 'avans',
      email: 'test@avans.nl',
      password: '123456'
    }
    registerForm(validUser.name, validUser.email, validUser.password)
    expect(component.registerForm.value).toEqual(validUser)
  })

  it('Form invalid should be true when form is invalid', () => {
    const blankUser = {
      name: '',
      email: '',
      password: ''
    }

    registerForm(blankUser.name, blankUser.email, blankUser.password)
    expect(component.registerForm.invalid).toBeTruthy()
  })
})
