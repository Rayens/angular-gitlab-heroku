import { TestBed } from '@angular/core/testing'

import { AuthService } from './auth.service'
import { JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt'

describe('AuthService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [AuthService, JwtHelperService, { provide: JWT_OPTIONS, useValue: JWT_OPTIONS }]
    })
  )

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService)
    expect(service).toBeTruthy()
  })
})
