import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { User } from '../../domain/User'

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private http: HttpClient) {}
  private userUrl = '/api/user'

  getAll() {
    return this.http.get<User[]>(`${this.userUrl}`)
  }

  register(user: User) {
    return this.http.post(`${this.userUrl}`, user)
  }

  update(user: { password: any; _id: string }) {
    return this.http.put(`${this.userUrl}/${user._id}`, user)
  }

  delete(id) {
    return this.http.delete(`${this.userUrl}/${id}`)
  }
}
