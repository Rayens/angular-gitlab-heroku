import { Component, OnInit } from '@angular/core'
import { User } from '../../domain/User'
import { Subscription } from 'rxjs'
import { AuthenticationService } from '../_services/authentication.service'
import { UserService } from '../_services/user.service'
import { first } from 'rxjs/operators'
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms'
import { Router } from '@angular/router'

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.scss']
})
export class PortalComponent implements OnInit {
  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user
    })
  }
  currentUser: User
  currentUserSubscription: Subscription
  updateForm: FormGroup
  loading = false
  submitted = false

  ngOnInit() {
    this.updateForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.updateForm.controls
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.currentUserSubscription.unsubscribe()
  }

  deleteUser() {
    this.userService
      .delete(this.currentUser._id)
      .pipe(first())
      .subscribe()
    this.authenticationService.logout()
    this.router.navigate(['/login'])
  }

  onSubmit() {
    this.submitted = true

    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return
    }

    this.loading = true
    this.userService
      .update({ _id: this.currentUser._id, password: this.updateForm.value })
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/login'])
        },
        error => {
          this.loading = false
        }
      )
  }
}
