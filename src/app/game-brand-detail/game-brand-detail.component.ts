import { Component, Input, OnInit } from '@angular/core'
import { Location } from '@angular/common'
import { GameBrandService } from '../game-brand.service'
import { ActivatedRoute } from '@angular/router'
import { GameBrand } from '../domain/GameBrand'

@Component({
  selector: 'app-game-brand-detail',
  templateUrl: './game-brand-detail.component.html',
  styleUrls: ['./game-brand-detail.component.scss']
})
export class GameBrandDetailComponent implements OnInit {
  @Input() gamebrand: GameBrand

  constructor(private route: ActivatedRoute, private location: Location, private gs: GameBrandService) {}

  goBack(): void {
    this.location.back()
  }

  getGamebrandId(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.gs.getGamebrandId(id).subscribe(gamebrands => (this.gamebrand = gamebrands))
  }

  deletegamebrand(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.gs.deleteGamebrand(id).subscribe()
    this.goBack()
  }

  ngOnInit() {
    this.getGamebrandId()
  }
}
