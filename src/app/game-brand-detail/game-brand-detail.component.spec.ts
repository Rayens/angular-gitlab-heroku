import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { GameBrandDetailComponent } from './game-brand-detail.component'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('GameBrandDetailComponent', () => {
  let component: GameBrandDetailComponent
  let fixture: ComponentFixture<GameBrandDetailComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameBrandDetailComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBrandDetailComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
