import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ConsoleCreateComponent } from './console-create.component'
import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule,
  FormsModule,
  ControlContainer
} from '@angular/forms'
import { NgModule } from '@angular/core'
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'

describe('ConsoleCreateComponent', () => {
  let component: ConsoleCreateComponent
  let fixture: ComponentFixture<ConsoleCreateComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsoleCreateComponent],
      imports: [DatePickerModule, FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      providers: [
        { provide: ControlContainer },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): string {
                  return '123'
                }
              }
            }
          }
        },
        { provide: Location }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsoleCreateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
