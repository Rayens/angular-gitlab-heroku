import { Component, OnInit, ViewChild } from '@angular/core'
import { ConsoleService } from '../console.service'
import { DatePickerComponent } from '@syncfusion/ej2-angular-calendars'
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms'
import { Location } from '@angular/common'

@Component({
  selector: 'app-console-create',
  templateUrl: './console-create.component.html',
  styleUrls: ['./console-create.component.scss']
})
export class ConsoleCreateComponent implements OnInit {
  angForm: FormGroup
  ejDatePicker: DatePickerComponent

  constructor(private fb: FormBuilder, private cs: ConsoleService, private location: Location) {
    this.createForm()
  }

  goBack(): void {
    this.location.back()
  }

  createForm() {
    this.angForm = this.fb.group({
      console_name: ['', Validators.required],
      console_brand: ['', Validators.required],
      console_model: ['', Validators.required],
      console_hdd: ['', Validators.required],
      console_processor: ['', Validators.required],
      console_ram: ['', Validators.required],
      console_date: ['', Validators.required],
      console_description: ['', Validators.required]
    })
  }

  get datepicker() {
    return this.angForm.get('console_date')
  }

  addConsole(
    // tslint:disable-next-line:variable-name
    console_name,
    // tslint:disable-next-line:variable-name
    console_brand,
    // tslint:disable-next-line:variable-name
    console_model,
    // tslint:disable-next-line:variable-name
    console_hdd,
    // tslint:disable-next-line:variable-name
    console_processor,
    // tslint:disable-next-line:variable-name
    console_ram,
    // tslint:disable-next-line:variable-name
    console_date,
    // tslint:disable-next-line:variable-name
    console_description
  ) {
    this.cs.addConsole(
      console_name,
      console_brand,
      console_model,
      console_hdd,
      console_processor,
      console_ram,
      console_date,
      console_description
    )
    this.goBack()
  }

  ngOnInit() {}
}
