import { Injectable } from '@angular/core'
import { GameBrand } from './domain/GameBrand'
import { Observable, observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Console } from './domain/Console'

@Injectable({
  providedIn: 'root'
})
export class GameBrandService {
  private gameBrandUrl = 'api/gamebrand'

  constructor(private http: HttpClient) {}

  addGamebrand(
    gamebrandName,
    gamebrandOwner,
    gamebrandIntroductionyear,
    gamebrandDescription,
    gamebrandHistory
  ) {
    const gameBrand = {
      name: gamebrandName,
      owner: gamebrandOwner,
      introductionYear: gamebrandIntroductionyear,
      description: gamebrandDescription,
      history: gamebrandHistory
    }
    this.http.post(this.gameBrandUrl, gameBrand).subscribe()
  }

  getGamebrand(): Observable<GameBrand[]> {
    return this.http.get<GameBrand[]>(this.gameBrandUrl)
  }

  getGamebrandId(id): Observable<GameBrand> {
    const url = `${this.gameBrandUrl}/${id}`
    return this.http.get<GameBrand>(url)
  }

  deleteGamebrand(id): Observable<GameBrand> {
    const url = `${this.gameBrandUrl}/${id}`
    return this.http.delete<GameBrand>(url)
  }

  updateGamebrand(
    id,
    gamebrandName,
    gamebrandOwner,
    gamebrandIntroductionyear,
    gamebrandDescription,
    gamebrandHistory
  ) {
    const url = `${this.gameBrandUrl}/${id}`
    const UpdatedGamebrand = {
      name: gamebrandName,
      owner: gamebrandOwner,
      introductionYear: gamebrandIntroductionyear,
      description: gamebrandDescription,
      history: gamebrandHistory
    }
    this.http.put(url, UpdatedGamebrand).subscribe()
  }
}
