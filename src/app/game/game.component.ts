import { Component, OnInit } from '@angular/core'
import { Game } from '../domain/Game'
import { GameService } from '../game.service'

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  game: Game[]

  constructor(private gs: GameService) {}

  getGame(): void {
    this.gs.getGame().subscribe(games => (this.game = games))
  }

  ngOnInit() {
    this.getGame()
  }
}
