import { Injectable } from '@angular/core'
import { Console } from './domain/Console'
import { Observable, observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, map, tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ConsoleService {
  private consoleUrl = '/api/console'

  constructor(private http: HttpClient) {}

  // tslint:disable-next-line:variable-name
  addConsole(
    // tslint:disable-next-line:variable-name
    console_name,
    // tslint:disable-next-line:variable-name
    console_brand,
    // tslint:disable-next-line:variable-name
    console_model,
    // tslint:disable-next-line:variable-name
    console_hdd,
    // tslint:disable-next-line:variable-name
    console_processor,
    // tslint:disable-next-line:variable-name
    console_ram,
    // tslint:disable-next-line:variable-name
    console_date,
    // tslint:disable-next-line:variable-name
    console_description
  ) {
    const console = {
      name: console_name,
      brand: console_brand,
      model: console_model,
      hdd: console_hdd,
      processor: console_processor,
      ram: console_ram,
      released: console_date,
      description: console_description
    }
    this.http.post(this.consoleUrl, console).subscribe()
  }

  getConsole(): Observable<Console[]> {
    return this.http.get<Console[]>(this.consoleUrl)
  }

  getConsoleId(id): Observable<Console> {
    const url = `${this.consoleUrl}/${id}`
    return this.http.get<Console>(url)
  }

  deleteConsole(id): Observable<Console> {
    const url = `${this.consoleUrl}/${id}`

    return this.http.delete<Console>(url)
  }

  // tslint:disable-next-line:variable-name max-line-length
  updateConsole(
    id,
    // tslint:disable-next-line:variable-name
    console_name,
    // tslint:disable-next-line:variable-name
    console_brand,
    // tslint:disable-next-line:variable-name
    console_model,
    // tslint:disable-next-line:variable-name
    console_hdd,
    // tslint:disable-next-line:variable-name
    console_processor,
    // tslint:disable-next-line:variable-name
    console_ram,
    // tslint:disable-next-line:variable-name
    console_date,
    // tslint:disable-next-line:variable-name
    console_description
  ) {
    const url = `${this.consoleUrl}/${id}`
    const updateConsole = {
      name: console_name,
      brand: console_brand,
      model: console_model,
      hdd: console_hdd,
      processor: console_processor,
      ram: console_ram,
      released: console_date,
      description: console_description
    }
    return this.http.put(url, updateConsole).subscribe()
  }
}
