import { Component, Input } from '@angular/core'
import { AppComponent } from '../app/app.component'

@Component({
  selector: 'app-navbar',
  template: `
    <nav class="navbar navbar-expand-md navbar-dark bg-danger fixed-top">
      <div class="container">
        <a
          class="navbar-brand"
          routerLink="/"
          [routerLinkActive]="['active']"
          [routerLinkActiveOptions]="{ exact: true }"
          >{{ title }}</a
        >
        <button
          class="navbar-toggler hidden-sm-up"
          type="button"
          (click)="isNavbarCollapsed = !isNavbarCollapsed"
          data-target="#navbarsDefault"
          aria-controls="navbarsDefault"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div [ngbCollapse]="isNavbarCollapsed" class="collapse navbar-collapse" id="navbarsDefault">
          <ul class="navbar-nav mr-auto">
            <!--                      <li class="nav-item">-->
            <!--                          <a class="nav-link" routerLink="/" [routerLinkActive]="['active']">Games</a>-->
            <!--                      </li>-->
            <li class="nav-item">
              <a class="nav-link" routerLink="game" [routerLinkActive]="['active']">Games</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="console" [routerLinkActive]="['active']">Consoles</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="gamebrand" [routerLinkActive]="['active']">Game Brands</a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                routerLink="about"
                [routerLinkActive]="['active']"
                [routerLinkActiveOptions]="{ exact: true }"
                >About</a
              >
            </li>
          </ul>
          <ul *ngIf="appComponent.currentUser" ngbDropdown class=" navbar-nav d-inline-block">
            <button class="btn btn-outline-primary" id="dropdownBasic1" ngbDropdownToggle>
              {{ appComponent.currentUser.name }}
            </button>
            <div ngbDropdownMenu aria-labelledby="dropdownBasic1">
              <button
                class="dropdown-item"
                routerLink="portal"
                [routerLinkActive]="['active']"
                [routerLinkActiveOptions]="{ exact: true }"
              >
                Account overview
              </button>
              <button class="dropdown-item" (click)="appComponent.logout()">Logout</button>
            </div>
          </ul>
          <ul class="navbar-nav">
            <li *ngIf="!appComponent.currentUser" class="nav-item">
              <a
                class="nav-link"
                routerLink="login"
                [routerLinkActive]="['active']"
                [routerLinkActiveOptions]="{ exact: true }"
                >Login</a
              >
            </li>
          </ul>
        </div>
      </div>
    </nav>
  `,
  styles: [
    '.btn-link { color: rgba(255,255,255,.5); text-decoration: none; }',
    // tslint:disable-next-line: max-line-length
    '.btn-link.focus, .btn-link:focus, .btn-link.hover, .btn-link:hover { color: rgba(255,255,255,.75); text-decoration: none; box-shadow: none; }'
  ]
})
export class NavbarComponent {
  @Input() title: string
  isNavbarCollapsed = true

  constructor(public appComponent: AppComponent) {}
}
