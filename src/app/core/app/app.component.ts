import { Component } from '@angular/core'
import { User } from '../../domain/User'
import { Router } from '@angular/router'
import { AuthenticationService } from '../../authentication/_services/authentication.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Game Studio'

  currentUser: User

  constructor(private router: Router, private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => (this.currentUser = x))
  }

  logout() {
    this.authenticationService.logout()
    this.router.navigate(['/login'])
  }
}
