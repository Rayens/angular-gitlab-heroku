import { BrowserModule } from '@angular/platform-browser'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { GameComponent } from './game/game.component'
import { ConsoleComponent } from './console/console.component'
import { GameBrandComponent } from './game-brand/game-brand.component'
import { ConsoleDetailComponent } from './console-detail/console-detail.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ConsoleCreateComponent } from './console-create/console-create.component'
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars'
import { ConsoleUpdateComponent } from './console-update/console-update.component'
import { GameBrandCreateComponent } from './game-brand-create/game-brand-create.component'
import { GameBrandDetailComponent } from './game-brand-detail/game-brand-detail.component'
import { GameBrandUpdateComponent } from './game-brand-update/game-brand-update.component'
import { GameDetailComponent } from './game-detail/game-detail.component'
import { GameCreateComponent } from './game-create/game-create.component'
import { GameUpdateComponent } from './game-update/game-update.component'
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns'
import { LoginComponent } from './authentication/login/login.component'
import { RegisterComponent } from './authentication/register/register.component'
import { JwtInterceptor } from './_helpers/jwt.interceptor'
import { ErrorInterceptor } from './_helpers/error.interceptor'
import { AuthService } from './authentication/auth.service'
import { AuthGuardService } from './authentication/auth-guard.service'
import { JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt'
import { PortalComponent } from './authentication/portal/portal.component'
import { Error403Component } from './authentication/error/error403/error403.component'
import { Error404Component } from './authentication/error/error404/error404.component'
import { Error500Component } from './authentication/error/error500/error500.component'

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    DashboardComponent,
    GameComponent,
    ConsoleComponent,
    GameBrandComponent,
    ConsoleDetailComponent,
    ConsoleCreateComponent,
    ConsoleUpdateComponent,
    GameBrandCreateComponent,
    GameBrandDetailComponent,
    GameBrandUpdateComponent,
    GameDetailComponent,
    GameCreateComponent,
    GameUpdateComponent,
    LoginComponent,
    RegisterComponent,
    PortalComponent,
    Error403Component,
    Error404Component,
    Error500Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DatePickerModule,
    MultiSelectModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    AuthService,
    AuthGuardService,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
