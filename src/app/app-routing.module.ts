import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { GameComponent } from './game/game.component'
import { ConsoleComponent } from './console/console.component'
import { GameBrandComponent } from './game-brand/game-brand.component'
import { ConsoleDetailComponent } from './console-detail/console-detail.component'
import { ConsoleCreateComponent } from './console-create/console-create.component'
import { ConsoleUpdateComponent } from './console-update/console-update.component'
import { GameBrandCreateComponent } from './game-brand-create/game-brand-create.component'
import { GameBrandDetailComponent } from './game-brand-detail/game-brand-detail.component'
import { GameBrandUpdateComponent } from './game-brand-update/game-brand-update.component'
import { GameDetailComponent } from './game-detail/game-detail.component'
import { GameUpdateComponent } from './game-update/game-update.component'
import { GameCreateComponent } from './game-create/game-create.component'
import { LoginComponent } from './authentication/login/login.component'
import { AuthGuardService } from './authentication/auth-guard.service'
import { RegisterComponent } from './authentication/register/register.component'
import { PortalComponent } from './authentication/portal/portal.component'
import { Error403Component } from './authentication/error/error403/error403.component'
import { Error404Component } from './authentication/error/error404/error404.component'
import { Error500Component } from './authentication/error/error500/error500.component'

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: UsecasesComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'portal', component: PortalComponent },
  { path: 'game', component: GameComponent },
  { path: 'game/detail/:id', component: GameDetailComponent },
  { path: 'game/detail/:id/update', component: GameUpdateComponent, canActivate: [AuthGuardService] },
  { path: 'game/create', component: GameCreateComponent, canActivate: [AuthGuardService] },
  { path: 'console', component: ConsoleComponent },
  { path: 'console/detail/:id', component: ConsoleDetailComponent },
  { path: 'console/detail/:id/update', component: ConsoleUpdateComponent, canActivate: [AuthGuardService] },
  { path: 'console/create', component: ConsoleCreateComponent, canActivate: [AuthGuardService] },
  { path: 'gamebrand', component: GameBrandComponent },
  { path: 'gamebrand/detail/:id', component: GameBrandDetailComponent },
  {
    path: 'gamebrand/detail/:id/update',
    component: GameBrandUpdateComponent,
    canActivate: [AuthGuardService]
  },
  { path: 'gamebrand/create', component: GameBrandCreateComponent, canActivate: [AuthGuardService] },
  { path: 'error/403', component: Error403Component },
  { path: 'error/404', component: Error404Component },
  { path: 'error/500', component: Error500Component },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
