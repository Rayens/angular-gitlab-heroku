const mongoose = require('mongoose')
const schema = mongoose.Schema

// Creating the gamebrand schema
const gameBrandSchema = new schema({
  name: {
    type: String,
    required: [true, 'Creating a gamebrand requires a name']
  },
  owner: {
    type: String,
    required: [true, 'Creating a gamebrand requires a owner']
  },
  introductionYear: {
    type: Date
  },
  description: {
    type: String,
    required: [true, 'Creating a gamebrand requires a description']
  },
  history: {
    type: String,
    required: [true, 'Creating a gamebrand requires history']
  },
  user: {
    type: schema.Types.ObjectId,
    ref: 'user'
  }
})

// Creating the model
const gamebrand = mongoose.model('gamebrand', gameBrandSchema)

// Exporting the model
module.exports = gamebrand
