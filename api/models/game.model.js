const mongoose = require('mongoose')
const schema = mongoose.Schema

const GameSchema = new schema({
  name: {
    type: String,
    required: [true, 'Creating a game requires a name']
  },
  ean: {
    type: Number,
    required: [true, 'Creating a game requires a EAN']
  },
  console: {
    type: schema.Types.ObjectId,
    ref: 'console'
  },
  pegi: {
    type: Number,
    required: [true, 'Creating a game requires pegi']
  },
  genre: {
    type: String,
    required: [true, 'Creating a game requires a genre']
  },
  gamebrand: {
    type: schema.Types.ObjectId,
    ref: 'gamebrand'
  },
  released: {
    type: Date,
    required: [true, 'Creating a game requires a release date']
  },
  description: {
    type: String,
    required: [true, 'Creating a game requires a description']
  },
  user: {
    type: schema.Types.ObjectId,
    ref: 'user'
  }
})

const Game = mongoose.model('game', GameSchema)

// Exporting the model
module.exports = Game
