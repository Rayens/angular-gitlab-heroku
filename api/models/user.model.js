const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const saltrounds = 10
const schema = mongoose.Schema

const UserSchema = new schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  }
})

// Hashing the user before saving it
UserSchema.pre('save', function(next) {
  this.password = bcrypt.hashSync(this.password, saltrounds)
  next()
})

// Saving the model
const User = mongoose.model('user', UserSchema)

// Exporting the model
module.exports = User
