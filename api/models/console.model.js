const mongoose = require('mongoose')
const schema = mongoose.Schema

const ConsoleSchema = new schema({
  name: {
    type: String,
    required: [true, 'creating a console requires a username ']
  },
  brand: {
    type: String,
    required: [true, 'creating a console requires a brand']
  },
  model: {
    type: String,
    required: [true, 'creating a console requires a model']
  },
  hdd: {
    type: String,
    required: [true, 'creating a console requires a hdd']
  },
  processor: {
    type: String,
    required: [true, 'creating a console requires a processor']
  },
  ram: {
    type: String,
    required: [true, 'creating a console requires ram']
  },
  released: {
    type: Date
  },
  description: {
    type: String,
    required: [true, 'creating a console requires an description']
  },
  user: {
    type: schema.Types.ObjectId,
    ref: 'user'
  }
})

// Creating Console model
const Console = mongoose.model('console', ConsoleSchema)

// Exporting the Console
module.exports = Console
