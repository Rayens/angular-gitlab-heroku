const express = require('express')
const router = express.Router()
const gameController = require('../controllers/game.controller')
const userController = require('../controllers/user.controller')

// Getting games
router.get('/', gameController.getGames)

// Getting game by id
router.get('/:id', gameController.getGameId)

// Creating new game
router.post('/', userController.validateToken, gameController.createGame)

// Updating game by id
router.put('/:id', userController.validateToken, gameController.updateGame)

// Delete game by id
router.delete('/:id', userController.validateToken, gameController.deleteGame)

// Exporting the router
module.exports = router
