const express = require('express')
const router = express.Router()
const userController = require('../controllers/user.controller')

// Creating a new user
router.post('/', userController.createUser)

// Creating a new user
router.put('/:id', userController.updateUser)

// Delete user by ID
router.delete('/:id', userController.deleteUser)

// Posting a login
router.post('/authenticate', userController.login)

// Exporting the router
module.exports = router
