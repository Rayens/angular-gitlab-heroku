const express = require('express')
const router = express.Router()
const consoleController = require('../controllers/console.controller')
const userController = require('../controllers/user.controller')

// Getting all consoles
router.get('/', consoleController.getConsoles)

// Getting specific console
router.get('/:id', consoleController.getConsoleId)

// Secured routes (requires login)
// Post console
router.post('/', userController.validateToken, consoleController.createConsole)

// Update Console
router.put('/:id', userController.validateToken, consoleController.updateConsole)

// Delete Console
router.delete('/:id', userController.validateToken, consoleController.deleteConsole)

// Exporting the router
module.exports = router
