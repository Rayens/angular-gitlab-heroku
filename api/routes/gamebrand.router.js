const express = require('express')
const router = express.Router()
const gameBrandController = require('../controllers/gamebrand.controller')
const userController = require('../controllers/user.controller')

router.get('/', gameBrandController.getGameBrand)

router.get('/:id', gameBrandController.getGameBrandId)

// Creating new GameBrand
router.post('/', userController.validateToken, gameBrandController.createGameBrand)

// Updating gamebrand
router.put('/:id', userController.validateToken, gameBrandController.updateGameBrand)

// Deleteing gamebrand
router.delete('/:id', userController.validateToken, gameBrandController.deleteGameBrand)

// Exporting the router
module.exports = router
