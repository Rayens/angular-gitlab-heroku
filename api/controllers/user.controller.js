const User = require('../models/user.model')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const logger = require('../config/logger').logger

let functions = {}

functions.createUser = async (req, res, next) => {
  logger.info('Create user summoned')

  const user = req.body
  await User.create(new User(user))
    .then(result => res.status(200).send(user))
    .catch(err => res.send(err))
}

functions.updateUser = async (req, res, next) => {
  logger.info('Put user by id summoned')
  logger.trace(req.params.id)
  logger.fatal(req.body._id)
  logger.fatal(req.body.password.password)

  const user = req.body
  await User.findById(user._id)
    .then(model => {
      return Object.assign(model, {
        password: user.password.password
      })
    })
    .then(model => {
      return model.save()
    })
    .then(updatedModel => {
      res.json({
        msg: 'model updated',
        updatedModel
      })
    })
    .catch(err => {
      res.send(err)
    })
}

functions.deleteUser = async (req, res, next) => {
  logger.info('delete user by id summoned')
  const userId = req.params.id
  console.log(req.params.id)
  if (!userId) {
    res.status(404).end()
    return
  }

  await User.findOneAndDelete({ _id: userId })
    .then(result => {
      res.status(200).send(userId + ' deleted!')
      logger.trace(userId)
    })
    .catch(err => {
      res.send(err)
    })
}

functions.login = async (req, res, next) => {
  logger.info('User login summoned')

  const user = req.body

  await User.findOne({ email: user.email })
    .then(result => {
      console.log(result)
      if (bcrypt.compareSync(user.password, result.password)) {
        const token = jwt.sign({ id: result._id }, req.app.get('secretkey'))
        res.status(200).json({
          status: 'success',
          message: 'User found',
          data: {
            user: result,
            token: token
          }
        })
      } else {
        res.status(401).json({
          status: 'error',
          message: 'Invalid email/password',
          data: null
        })
      }
    })
    .catch(err => {
      res.status(401).send('Wrong email!')
      logger.error(err)
    })
}

// Validate Token Verification
functions.validateToken = (req, res, next) => {
  logger.info('validateToken called')
  const authHeader = req.headers.authorization
  console.log(req.headers)
  if (!authHeader) {
    res.status(403)
    errorObject = {
      message: 'Authorization header missing!',
      code: 401
    }
    logger.warn('Failed to validate token: ', errorObject.message)
    return next(errorObject)
  }
  const token = authHeader.substring(7, authHeader.length)

  jwt.verify(token, req.app.get('secretkey'), (err, payload) => {
    if (err) {
      res.status(403)
      errorObject = {
        message: 'Not authorized',
        code: 401
      }
      logger.warn('Failed to validate token: ', errorObject.message)
      next(errorObject)
    }
    if (payload) {
      logger.debug('Token is valid', payload)
      // User has access. Add UserId from payload to request so it can be re-used in future calls
      req.user = payload.data
      next()
    }
  })
}

// Exporting the functions
module.exports = functions
