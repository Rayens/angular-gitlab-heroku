const GameBrand = require('../models/gamebrand.model')
const logger = require('../config/logger').logger
const jwt = require('jsonwebtoken')

let functions = {}

functions.createGameBrand = async (req, res, next) => {
  logger.info('Post gamebrand summoned')

  // Getting the id out of the token
  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  const gamebrand = new GameBrand(req.body)
  await gamebrand
    .save()
    .then(result => {
      res.status(200).json({
        status: true,
        message: gamebrand
      })
      logger.trace(gamebrand)
    })
    .catch(err => {
      res.send(err)
      logger.error(err)
    })
}

functions.getGameBrand = async (req, res, next) => {
  logger.info('Get all gamebrand summoned')
  // Finding all gamebrands
  const gamebrands = await GameBrand.find()
    .populate('gamebrands')
    .then(result => {
      res.status(200).send(result)
      logger.trace(result)
    })
    .catch(error => {
      res.status(404).json({
        status: false,
        message: error
      })
      logger.error(error)
    })
}

functions.getGameBrandId = async (req, res, next) => {
  logger.info('Get 1 gamebrand summoned')
  // Defining variables
  const gamebrandId = req.params.id

  await GameBrand.find({ _id: gamebrandId })
    .then(result => {
      const date = result[0].introductionYear.toLocaleString().split(' ')
      const realDate = date[0]
      res.status(200).json({
        _id: result[0]._id,
        name: result[0].name,
        owner: result[0].owner,
        introductionYear: realDate,
        description: result[0].description,
        history: result[0].history
      })
      logger.trace(result[0])
    })
    .catch(error => {
      res.status(404).json({
        status: false,
        message: error
      })
      logger.error(error)
    })
}

functions.updateGameBrand = async (req, res, next) => {
  logger.info('PUT by ID summoned')

  const content = req.body
  const gamebrandId = req.params.id

  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  await GameBrand.findById(gamebrandId)
    .then(model => {
      if (req.body.user !== model.user.toString()) {
        res.status(403).end({
          message: 'Not allowed to modify other users entity!',
          code: 403
        })
      }
      return Object.assign(model, {
        name: content.name,
        owner: content.owner,
        introductionYear: content.introductionYear,
        description: content.description,
        history: content.history
      })
    })
    .then(model => {
      return model.save()
    })
    .then(updatedModel => {
      res.json({
        msg: 'model updated',
        updatedModel
      })
    })
    .catch(err => {
      res.status(404).send(err)
    })
}

functions.deleteGameBrand = async (req, res, next) => {
  logger.info('DELETE gamebrand by id summoned')
  // Defining variables
  const gamebrandId = req.params.id

  // Getting the id out of the token
  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  // Trying to find the Gamebrand by ID
  try {
    // Searching by ID
    let foundgamebrand = await GameBrand.find({ _id: gamebrandId }).catch(function(error) {
      res.status(404).send(error)
      logger.error(error)
    })
    if (req.body.user !== foundgamebrand[0].user.toString()) {
      res.status(403).end({
        message: 'Not allowed to modify other users entity!',
        code: 403
      })
    }

    // If there is no Gamebrand a bad request is send
    try {
      await GameBrand.deleteOne({ _id: gamebrandId }).catch(function(error) {
        res.status(404).send(error)
        logger.error(error)
      })
      // Response status
      res.status(200).send(foundgamebrand[0])
      // Error handling
    } catch (err) {
      res.send(err)
    }
    // Error handling
  } catch (error) {
    res.status(204).end('Id doesnt exist')
  }
}

// Exporting the functions
module.exports = functions
