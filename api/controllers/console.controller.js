const Console = require('../models/console.model')
const logger = require('../config/logger').logger
const jwt = require('jsonwebtoken')

let functions = {}
functions.createConsole = async (req, res, next) => {
  // Getting the id out of the token
  const authorization = req.headers.authorization.split(' ')[1]
  let userId

  try {
    userId = jwt.verify(authorization, req.app.get('secretkey'))
    req.body.user = userId.id
    logger.info('POST Console summoned')
    // saving the Console
    const console = new Console(req.body)
    logger.info(req.body)
    // tracing received object
    logger.trace(console)
    await console
      .save()
      .then(res.status(200).send({ console }))
      .catch(function(err) {
        res.status(500).end('Something went wrong')
        logger.error(err)
      })
    // Error Handling
  } catch (err) {
    res.status(404).end()
    logger.error(err)
  }

  logger.info('POST succesful')
}

functions.getConsoles = async (req, res, next) => {
  logger.info('GET all Consoles summoned')
  // Finding all Consoles
  const consoles = await Console.find()
    .populate('consoles')
    .catch(function(error) {
      res.status(404).send(error)
      logger.error(error)
    })
  // Storing Console modified in result
  logger.trace(consoles)

  // Response status
  res.status(200).send(consoles)
}

functions.getConsoleId = async (req, res, next) => {
  logger.info('GET console by ID summoned')
  // Defining variables
  const consoleId = req.params.id
  // Finding specific console
  await Console.find({ _id: consoleId })
    .then(result => {
      const date = result[0].released.toLocaleString().split(' ')
      const realDate = date[0]
      res.status(200).json({
        _id: result[0]._id,
        name: result[0].name,
        brand: result[0].brand,
        model: result[0].model,
        hdd: result[0].hdd,
        processor: result[0].processor,
        ram: result[0].ram,
        released: realDate,
        description: result[0].description
      })
      logger.trace(result[0])
    })
    .catch(error => {
      res.status(400).json({
        status: false,
        message: error
      })
      logger.error(error)
    })
}

functions.updateConsole = async (req, res, next) => {
  logger.info('PUT by ID summoned')

  const content = req.body
  const consoleId = req.params.id

  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  await Console.findById(consoleId)
    .then(model => {
      if (req.body.user !== model.user.toString()) {
        res.status(403).end({
          message: 'Not allowed to modify other users entity!',
          code: 403
        })
      }
      return Object.assign(model, {
        name: content.name,
        brand: content.brand,
        model: content.model,
        hdd: content.hdd,
        processor: content.processor,
        ram: content.ram,
        released: content.released,
        description: content.description
      })
    })
    .then(model => {
      return model.save()
    })
    .then(updatedModel => {
      res.json({
        msg: 'model updated',
        updatedModel
      })
    })
    .catch(err => {
      res.send(err)
    })
}

functions.deleteConsole = async (req, res, next) => {
  logger.info('DELETE console by id summoned')
  // Defining variables
  const consoleId = req.params.id

  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  // Trying to find the Console by ID
  try {
    // Searching by ID
    let foundconsole = await Console.find({ _id: consoleId }).catch(function(error) {
      res.status(404).send(error)
      logger.error(error)
    })

    console.log(req.body.user)
    console.log(foundconsole[0].user.toString)

    if (req.body.user !== foundconsole[0].user.toString()) {
      console.log('1')
      res.status(403).end({
        message: 'Not allowed to modify other users entity!',
        code: 403
      })
    }
    // If there is no console a bad request is send
    try {
      console.log('2')

      await Console.deleteOne({ _id: consoleId }).catch(function(error) {
        res.status(404).send(error)
        logger.error(error)
      })

      // Response status
      res.status(200).send(foundconsole[0])
      // Error handling
    } catch (err) {
      res.send(err)
    }
    // Error handling
    console.log('3')

    // Error handling
  } catch (error) {
    res.status(204).end('Id doesnt exist')
  }
}

module.exports = functions
