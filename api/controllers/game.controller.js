const Console = require('../models/console.model')
const Game = require('../models/game.model')
const GameBrand = require('../models/gamebrand.model')
const logger = require('../config/logger').logger
const jwt = require('jsonwebtoken')

let functions = {}

functions.createGame = async (req, res, next) => {
  logger.info('Post game summoned')

  // Getting the id out of the token
  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  // getting the gamebrand id out of the body
  const splitter = req.body.gamebrand.split(' ')
  const gameBrand = splitter[1]

  // getting the console id out of the body
  const cSplitter = req.body.console.split(' ')
  const console = cSplitter[1]

  const game = new Game({
    name: req.body.name,
    ean: req.body.ean,
    console: console,
    pegi: req.body.pegi,
    genre: req.body.genre,
    gamebrand: gameBrand,
    released: req.body.released,
    description: req.body.description,
    user: req.body.user
  })
  await game
    .save()
    .then(result => {
      res.status(200).json({
        status: true,
        message: game
      })
      logger.trace(game)
    })
    .catch(err => {
      res.send(err)
      logger.error(err)
    })
  next()
}

functions.getGames = async (req, res, next) => {
  logger.info('Get all games summoned')
  // Finding all games
  await Game.find()
    .populate('games')
    .then(result => {
      res.status(200).send(result)
      logger.trace(result)
    })
    .catch(error => {
      res.status(404).json({
        status: false,
        message: error
      })
      logger.error(error)
    })
}

functions.getGameId = async (req, res, next) => {
  logger.info('Get 1 game summoned')
  // Defining variables
  const gameId = req.params.id

  const foundGame = await Game.find({ _id: gameId }).catch(error => {
    res.status(404).json({
      status: false,
      message: error
    })
    logger.error(error)
  })

  // getting the release date out of the body
  const g = foundGame[0].released.toLocaleString()
  const rSplitter = g.split(' ')
  let foundConsole
  // Finding console in game
  foundConsole = await Console.find({ _id: foundGame[0].console }).catch(error => {
    res.status(404).json({
      status: false,
      message: error
    })
    logger.error(error)
  })

  let gamebrand
  await GameBrand.find({ _id: foundGame[0].gamebrand }).then(result => {
    if (!result[0]) {
      gamebrand = 'Gamebrand not found'
    } else {
      gamebrand = result[0].name
    }
    if (!foundConsole[0]) {
      foundConsole = 'Console not found'
    } else {
      foundConsole = foundConsole[0].name
    }
    console.log(foundConsole)

    res.status(200).json({
      _id: foundGame[0]._id,
      name: foundGame[0].name,
      ean: foundGame[0].ean,
      console: foundConsole,
      pegi: foundGame[0].pegi,
      genre: foundGame[0].genre,
      gamebrand: gamebrand,
      released: rSplitter[0],
      description: foundGame[0].description
    })
    logger.trace(foundConsole)
  })
}

functions.updateGame = async (req, res, next) => {
  logger.info('PUT by ID summoned')

  const content = req.body
  const gameId = req.params.id

  // getting the gamebrand id out of the body
  const splitter = content.gamebrand.split(' ')
  const gameBrand = splitter[1]

  // getting the console id out of the body
  const cSplitter = content.console.split(' ')
  const console = cSplitter[1]

  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  await Game.findById(gameId)
    .then(model => {
      if (req.body.user !== model.user.toString()) {
        res.status(403).end({
          message: 'Not allowed to modify other users entity!',
          code: 403
        })
      }
      return Object.assign(model, {
        name: content.name,
        ean: content.ean,
        console: console,
        pegi: content.pegi,
        genre: content.genre,
        gamebrand: gameBrand,
        released: content.released,
        description: content.description
      })
    })
    .then(model => {
      return model.save()
    })
    .then(updatedModel => {
      res.json({
        msg: 'model updated',
        updatedModel
      })
    })
    .catch(err => {
      res.send(err)
    })
}

functions.deleteGame = async (req, res, next) => {
  logger.info('DELETE game by id summoned')
  // Defining variables
  const gameId = req.params.id

  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  await Game.findOne({ _id: gameId }).then(result => {
    if (req.body.user !== result.user.toString()) {
      res.status(403).end({
        message: 'Not allowed to modify other users entity!',
        code: 403
      })
    }
  })

  await Game.findOneAndDelete({ _id: gameId })
    .then(result => {
      res.status(200).send(gameId + ' deleted!')
      logger.trace(gameId)
    })
    .catch(err => {
      res.status(404).send(err)
    })
}

// Exporting the functions
module.exports = functions
