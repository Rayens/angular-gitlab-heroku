const express = require('express'),
  path = require('path'),
  bodyParser = require('body-parser'),
  cors = require('cors'),
  mongoose = require('mongoose'),
  config = require('./config/db'),
  logger = require('./config/logger').logger
http = require('http')
jwt = require('jsonwebtoken')

// Importing routes
const userRoutes = require('./routes/user.router')
const consoleRouter = require('./routes/console.router')
const gameRouter = require('./routes/game.router')
const gameBrandRouter = require('./routes/gamebrand.router')

// Connecting to the mongo database
mongoose.Promise = global.Promise
mongoose.connect(config.DB, { useNewUrlParser: true }).then(
  () => {
    logger.info('Database is connected')
  },
  err => {
    logger.info('Cannot connect to the database' + err)
  }
)

// Defining variables
const version = process.env.version || '1.0'
const app = express()
const appname = 'angular-gitlab-heroku'

// Setting jwt secret token
app.set('secretkey', 'nosql')
// app.use(session({secret : 'Rayen'}));

// Defining what express will use
app.use(bodyParser.json())
app.use(cors())
app.use(express.static(path.join(__dirname, '..', 'dist', appname)))

// Getting the version
app.get('/getversion', function(req, res) {
  logger.info('Version ' + version)
  res.status(200).json({ version: version })
})

// Base routes
app.use('/api/user', userRoutes)
app.use('/api/console', consoleRouter)
app.use('/api/game', gameRouter)
app.use('/api/gamebrand', gameBrandRouter)

// using the index.html
app.use('/', function(req, res) {
  res.sendFile(path.join(__dirname, `../dist/${appname}/`, 'index.html'))
})

// Get port from environment and store in Express.
const port = process.env.PORT || '3000'

// Create HTTP server.
//const server = http.createServer(app)

// Listen on provided port, on all network interfaces.
app.listen(port, () => logger.info(`Angular server \'${appname}\' running on port ${port}`))

module.exports = app
